import sm from "../../posts/thumbnails/sm.png";
import townhall from "../../posts/thumbnails/townhall.png";

export default [
  {
    id: "sitemonster",
    title: "The Secret Is Just Do a Little Bit Over a Long Time",
    path:
      "https://sitesmonster.com/interviews/tania-rascia-the-secret-is-just-do-a-little-bit-over-a-long-time",
    description: "Interview with James King on SitesMonster.com",
    staticThumbnail: sm,
  },
  {
    id: "hashnode",
    title: "She Inspires: Tania Rascia",
    path:
      "https://townhall.hashnode.com/women-in-tech-tania-rascia-ck2bfzn2100up3is10zhphol4",
    description: "Interview with Bolaji Ayodeji on Hashnode Townhall",
    staticThumbnail: townhall,
  },
];
